<?php

use Illuminate\Database\Seeder;
use App\Product ;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run( ) {
		for ( $i = 0 ; $i < 1e2 ; $i ++ ) {
			$product = new Product( ) ;
			$product->title = Str::random( rand( 10 , 60 ) ) ;
			$product->image_url = 'https://' . sha1( $i ) . '/' ;
			$product->price = rand( 1 , 1e6 ) / 1e2 ;
			$product->availability = rand( 1 , 1e8 ) ;
			$product->save( ) ;
		}
    }
}