<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductBasket extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up( ) {
		DB::statement( "
CREATE TABLE IF NOT EXISTS `product`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`title` VARCHAR( 200 ) NOT null COMMENT 'название' ,
	`image_url` VARCHAR( 1000 ) NOT null COMMENT 'картинка' ,
	`price` DECIMAL( 10 , 2 ) UNSIGNED NOT null COMMENT 'цена' ,
	`availability` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'наличие' ,
	`created_at` DATETIME NOT null COMMENT 'дата-время создания' ,
	`updated_at` DATETIME NOT null COMMENT 'дата-время изменения' ,

	PRIMARY KEY( `id` ) ,
	INDEX( `title` ) ,
	INDEX( `price` ) ,
	INDEX( `availability` )
) COMMENT 'товар' ;
		" ) ;

		DB::statement( "
CREATE TABLE IF NOT EXISTS `cart`(
	`id` BIGINT( 22 ) UNSIGNED NOT null AUTO_INCREMENT COMMENT 'идентификатор' ,
	`session_id` VARCHAR( 255 ) NOT null COMMENT 'идентификатор сессии пользователя' ,
	`product_id` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'идентификатор товара' ,
	`product_title` VARCHAR( 200 ) NOT null COMMENT 'название' ,
	`product_image_url` VARCHAR( 1000 ) NOT null COMMENT 'картинка' ,
	`product_price` DECIMAL( 10 , 2 ) NOT null COMMENT 'цена' ,
	`product_quantity` BIGINT( 22 ) UNSIGNED NOT null COMMENT 'количество' ,
	`created_at` DATETIME NOT null COMMENT 'дата-время создания' ,
	`updated_at` DATETIME NOT null COMMENT 'дата-время изменения' ,

	PRIMARY KEY( `id` ) ,
	INDEX( `session_id` ) ,

	CONSTRAINT `fk_cart_product_product_id`
		FOREIGN KEY ( `product_id` )
		REFERENCES `product`( `id` )
		ON DELETE CASCADE
		ON UPDATE CASCADE
) COMMENT 'корзина' ;
		" ) ;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down( ) {
		DB::statement( "DROP TABLE IF EXISTS `cart` ;" ) ;
		DB::statement( "DROP TABLE IF EXISTS `product` ;" ) ;
    }
}