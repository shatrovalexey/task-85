### ЗАДАЧА
```
https://docs.google.com/document/d/1ahfq20oymP-nA2E9DDEI8pWgwxgHB5-ihYTooJh6dXQ/edit
```

### РЕШЕНИЕ
```
composer install
php artisan key:generate
php artisan migrate --seed
php artisan serve
```

### АВТОР
Шатров Алексей Сергеевич <mail@ashatrov.ru>