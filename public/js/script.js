jQuery( function( ) {
	jQuery( ".ajax-form" ).on( "submit" , function( ) {
		let $self = jQuery( this ) ;

		jQuery.ajax( {
			"url" : $self.attr( "action" ) ,
			"data" : $self.serialize( ) ,
			"type" : $self.attr( "method" ) ,
			"dataType" : "json" ,
			"success" : function( $data ) {
				if ( "message" in $data ) {
					alert( $data.message ) ;

					return ;
				}

				alert( "Успешно" ) ;

				location.reload( ) ;
			} ,
			"error" : function( ) {
				alert( "Ошибка сервера" ) ;
			}
		} ) ;

		return false ;
	} ) ;
} ) ;