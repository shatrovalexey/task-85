<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;

use App\Product ;

class ProductController extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	* Список товаров
	*
	* @param Illuminate\Http\Request $request - запрос
	* @param string sorting - столбец для упорядочения
	* @param string sorting_array - порядок упорядочения столбца
	*/
	public function list( Request $request ) {
		$sorting = $request->input( 'sorting' ) ;
		$sorting_array = $request->input( 'sorting_array' ) ;
		$products = new Product( ) ;

		if ( empty( $sorting ) ) {
			$sorting = 'availability' ;
			$sorting_array = 'DESC' ;
		}
		if ( empty( $sorting_array ) ) {
			$sorting_array = 'ASC' ;
		}

		return view( 'product.list' , [
			'products' => $products->orderBy( $sorting , $sorting_array )->paginate( 10 ) ,
			'sorting' => $sorting ,
			'sorting_array' => $sorting_array ,
		] ) ;
	}
}