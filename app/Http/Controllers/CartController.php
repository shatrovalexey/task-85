<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use App\Cart ;
use App\Product ;

/**
* Корзина
*/
class CartController extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	/**
	* Список позиций корзины
	*
	* @param Illuminate\Http\Request $request - запрос
	* @param string sorting - столбец для упорядочения
	* @param string sorting_array - порядок упорядочения столбца
	*/
	public function list( Request $request ) {
		$sorting = $request->input( 'sorting' ) ;
		$sorting_array = $request->input( 'sorting_array' ) ;
		$cart_list = new Cart( ) ;

		if ( empty( $sorting ) ) {
			$sorting = 'product_title' ;
		}
		if ( empty( $sorting_array ) ) {
			$sorting_array = 'ASC' ;
		}

		return view( 'cart.list' , [
			'cart_list' => $cart_list->orderBy( $sorting , $sorting_array )->paginate( 10 ) ,
			'sorting' => $sorting ,
			'sorting_array' => $sorting_array ,
		] ) ;
	}

	/**
	* Добавление товара в корзину
	*
	* @param Illuminate\Http\Request $request - запрос
	* @param integer $product_id - идентификатор товара
	* @param integer $quantity - количество
	*/
	public function add( Request $request , int $product_id ) {
		$quantity = $request->get( 'quantity' ) ;
		$product = Product::find( $product_id ) ;

		try {
			if ( $quantity <= 0 ) {
				throw new \Exception( 'Указанное количество слишком мало' ) ;
			}
			if ( empty( $product ) ) {
				throw new \Exception( "Товар с $product_id не найден" ) ;
			}

			$session_id = $request->session( )->get( '_token' ) ;

			if ( $product->availability < $quantity ) {
				throw new \Exception( 'Не достаточно наличия' ) ;
			}

			DB::beginTransaction( ) ;

			$cart = Cart::where( 'product_id' , $product_id )->first( ) ;

			if ( empty( $cart ) ) {
				$cart = new Cart( ) ;
			}

			$cart->session_id = $session_id ;
			$cart->product_quantity = $quantity ;
			$cart->product_id = $product->id ;
			$cart->product_title = $product->title ;
			$cart->product_price = $product->price ;
			$cart->product_image_url = $product->image_url ;

			if ( ! $cart->save( ) ) {
				throw new \Exception( 'Не удалось добавить товар в корзину' ) ;
			}

			$product->availability -= $quantity ;

			if ( ! $product->save( ) ) {
				throw new \Exception( 'Не удалось обновить наличие' ) ;
			}

			DB::commit( ) ;

			return $cart ;
		} catch ( \Exception $exception ) {
			DB::rollback( ) ;

			return [
				'message' => $exception->getMessage( ) ,
			] ;
		}
	}

	/**
	* Удаление товара из корзины
	*
	* @param Illuminate\Http\Request $request - запрос
	* @param integer $cart_id - идентификатор позиции
	*/
	public function toggle( Request $request ) {
		$cart_id = $request->get( 'cart_id' ) ;
		$product_quantity = $request->get( 'product_quantity' ) ;

		try {
			if ( $product_quantity <= 0 ) {
				throw new \Exception( 'Указанное количество слишком мало' ) ;
			}

			$product_quantity_sum = Cart::where( 'id' , $cart_id )->sum( 'product_quantity' ) ;

			if ( $product_quantity_sum < $product_quantity ) {
				throw new \Exception( "Указанное количество слишком велико: $product_quantity_sum < $product_quantity" ) ;
			}

			DB::beginTransaction( ) ;

			$cart = Cart::where( 'id' , $cart_id )->first( ) ;

			if ( $product_quantity_sum == $product_quantity ) {
				$cart->delete( ) ;
			} else {
				$cart->product_quantity -= $product_quantity ;
				$cart->update( ) ;
			}

			$product = Product::where( 'id' , $cart->product_id )->first( ) ;
			$product->availability += $product_quantity ;
			$product->update( ) ;

			DB::commit( ) ;

			return [
				'cart' => $cart ,
				'product' => $product ,
			] ;
		} catch ( \Exception $exception ) {
			DB::rollback( ) ;

			return [
				'message' => $exception->getMessage( ) ,
			] ;
		}
	}
}