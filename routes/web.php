<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/' , 'ProductController@list' ) ;
Route::get( '/products' , 'ProductController@list' ) ;
Route::get( '/cart/add/{product_id}' , 'CartController@add' ) ;
Route::get( '/cart/list' , 'CartController@list' ) ;
Route::get( '/cart/toggle' , 'CartController@toggle' ) ;