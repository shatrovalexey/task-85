@extends('layouts.app')

@section('content')
<table class="table table-striped table-bordered table-hover">
	<caption>Товары</caption>
	<thead>
		<tr>
			<th>#</th>
			<th>
				@if ( ( $sorting == 'title' ) && ( $sorting_array == 'DESC' ) )
					<a href="?sorting=title">название &darr;</a>
				@else
					<a href="?sorting=title&amp;sorting_array=DESC">название &uarr;</a>
				@endif
			</th>
			<th>
				@if ( ( $sorting == 'price' ) && ( $sorting_array == 'DESC' ) )
					<a href="?sorting=price">цена &darr;</a>
				@else
					<a href="?sorting=price&amp;sorting_array=DESC">цена &uarr;</a>
				@endif
			</th>
			<th>
				@if ( ( $sorting == 'availability' ) && ( $sorting_array == 'DESC' ) )
					<a href="?sorting=availability">доступно &darr;</a>
				@else
					<a href="?sorting=availability&amp;sorting_array=DESC">доступно &uarr;</a>
				@endif
			</th>
			<th>
				в корзину
			</th>
		</tr>
	</thead>
	<tbody>
	@foreach ( $products as $product )
		<tr>
			<td>{{$product->id}}</td>
			<td>{{$product->title}}</td>
			<td>{{$product->price}}</td>
			<td>{{$product->availability}}</td>
			<td>
				<form action="/cart/add/{{$product->id}}" class="ajax-form">
					<input type="number" name="quantity" max="{{$product->availability}}" min="1" value="{{$product->availability}}">
					<input type="submit" value="&rarr;">
				</form>
			</td>
		</tr>
	@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="4">{{ $products->appends( request( )->except( 'page' ) )->links( ) }}</td>
		</tr>
	</tfoot>
</table>
@endsection