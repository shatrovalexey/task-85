@extends('layouts.app')

@section('content')
<table class="table table-striped table-bordered table-hover">
	<caption>Корзина</caption>
	<thead>
		<tr>
			<th>#</th>
			<th>
				@if ( ( $sorting == 'title' ) && ( $sorting_array == 'DESC' ) )
					<a href="?sorting=title">название &darr;</a>
				@else
					<a href="?sorting=title&amp;sorting_array=DESC">название &uarr;</a>
				@endif
			</th>
			<th>
				@if ( ( $sorting == 'price' ) && ( $sorting_array == 'DESC' ) )
					<a href="?sorting=price">цена &darr;</a>
				@else
					<a href="?sorting=price&amp;sorting_array=DESC">цена &uarr;</a>
				@endif
			</th>
			<th>
				@if ( ( $sorting == 'availability' ) && ( $sorting_array == 'DESC' ) )
					<a href="?sorting=availability">заказано &darr;</a>
				@else
					<a href="?sorting=availability&amp;sorting_array=DESC">заказано &uarr;</a>
				@endif
			</th>
			<th>
				<span>всего</span>
			</th>
			<th>
				удалить
			</th>
		</tr>
	</thead>
	<tbody>
	@foreach ( $cart_list as $cart_item )
		<tr>
			<td>{{$cart_item->id}}</td>
			<td>{{$cart_item->product_title}}</td>
			<td>{{$cart_item->product_price}}</td>
			<td>{{$cart_item->product_quantity}}</td>
			<td>{{$cart_item->product_price * $cart_item->product_quantity}}</td>
			<td>
				<form action="/cart/toggle" class="ajax-form">
					<input type="hidden" name="cart_id" value="{{$cart_item->id}}">
					<input type="number" name="product_quantity" max="{{$cart_item->product_quantity}}" min="1" value="{{$cart_item->product_quantity}}">
					<input type="submit" value="&rarr;">
				</form>
			</td>
		</tr>
	@endforeach
	</tbody>
	<tfoot>
		<tr>
			<td colspan="6">{{ $cart_list->appends( request( )->except( 'page' ) )->links( ) }}</td>
		</tr>
	</tfoot>
</table>
@endsection