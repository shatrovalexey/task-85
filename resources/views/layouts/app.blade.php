<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

		<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
		<script src="/js/script.js"></script>
    </head>
    <body>
		<ul>
			<li>
				<a href="/products">товары</a>
			</li>
			<li>
				<a href="/cart/list">корзина</a>
			</li>
		</ul>
		<div class="container">
			@yield('content')
		</div>
    </body>
</html>